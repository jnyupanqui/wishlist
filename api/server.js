const express = require('express');
const cors = require('cors');
const TravelDestination = require('./models/travel-destination');

const server = express();
const port = 3003;

server.use(cors());
server.use(express.json());

server.listen(port, () => {
    console.log(`Server running on ${port}`);
});

server.get('/destinos', (req, res) => {
    res.json(travelDestinations);
})

server.post('/destinos', (req, res) => {
    travelDestinations.push(req.body);
    res.json(travelDestinations);
});


/*
data
*/
var travelDestinations = [
    {"name": "Paris"},
    {"name": "Barcelona"},
    {"name": "Barranquilla"},
    {"name": "Montevideo"},
    {"name": "Santiago"},
    {"name": "Bogota"},
    {"name": "Lima"},
    {"name": "Quito"}
];