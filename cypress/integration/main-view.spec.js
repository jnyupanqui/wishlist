describe('ventana principal', () => {
    it('tiene encabezado correcto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Destinos de viaje');
        cy.get('h4').should('contain', 'Tracking');
    });
    it('informacion de credenciales invalidas', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('#inputUsername').type('user');
        cy.get('#inputPassword').type('bad');
        cy.get('button').click();
        cy.get('small.errorMsg').should('be.visible');
    });
    it('ocultar informacion de credenciales invalidas', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('#inputUsername').type('user');
        cy.get('#inputPassword').type('bad');
        cy.get('button').click();
        cy.get('small.errorMsg', { timeout: 5000 }).should('not.exist');
    });
    it('no login no acceso a manage', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('#inputUsername').type('user');
        cy.get('#inputPassword').type('bad');
        cy.get('button').click();
        cy.visit('http://localhost:4200/manage');
        cy.get('#inputName').should('not.exist');
    });
    it('login correcto con acceso a manage', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('#inputUsername').type('user');
        cy.get('#inputPassword').type('pass');
        cy.get('button').click();
        cy.visit('http://localhost:4200/manage');
        cy.get('#inputName').should('be.visible');
    });
    it('agregar nuevo destino', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('#inputUsername').type('user');
        cy.get('#inputPassword').type('pass');
        cy.get('button').click();
        cy.visit('http://localhost:4200/manage');
        cy.get('#inputName').type('Madrid');
        cy.get('form button').click();
        cy.get('.card p', { timeout: 2000 }).should('contain', 'Madrid');
    });
    it('mapa alert', () => {
        cy.visit('http://localhost:4200/mapa');
        cy.on ('window:alert', msg => alerted = msg);
        cy.get('.marker', { timeout: 5000 }).click()
        .then( () => expect(alerted).equal('Hola mundo!'));
    });
    it('tracking', () => {
        cy.visit('http://localhost:4200/home');
        cy.get('.card#Montevideo button').click();
        cy.get('.card#Montevideo button').click();
        cy.get('.card#Montevideo button').click();
        cy.get('.card#Montevideo button').click();
        cy.get('.card#Montevideo button').click();
        cy.get('ul li').should('contain', 'tag:#Montevideo, contador:5');
        
    });
    it('animation', () => {
        cy.visit('http://localhost:4200/home');
        cy.get('.card#Barcelona').should('have.css', 'background-color', 'rgb(255, 255, 255)');
        cy.get('.card#Barcelona button').click();
        cy.get('.card#Barcelona').should('have.css', 'background-color', 'rgb(175, 238, 238)');
        cy.get('.card#Paris button').click(); 
        cy.get('.card#Paris').should('have.css', 'background-color', 'rgb(175, 238, 238)');
        cy.get('.card#Barcelona').should('have.css', 'background-color', 'rgb(255, 255, 255)');
    });
    
});