import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, APP_INITIALIZER, Injectable } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TravelDestinationModule } from './travel-destination/travel-destination.module';
import { LoginComponent } from './login/login/login.component';

import { ApiClient } from './client/api.client';
import { ApiClientImpl } from './client/api-impl.client';
import { ApiClientLog } from './client/api-log.client';

import { AuthService } from './services/auth.service';
import { TrackingService } from './services/tracking.service';

import { TravelDestinationsState, initTravelDestination, travelDestinationReducer } from './models/travel-destination-state';
import { TrackingState, initTrackingState, trackingReducer} from './models/tracking-state';

import { ActionReducerMap, StoreModule as NGRXModule, Store } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';

import Dexie from 'dexie';
import { TravelDestination } from './models/travel-destination.model';
import { MapComponent } from './map/map/map.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TrackingDisplayComponent } from './tracking-display/tracking-display.component';

/*
LOCAL STORAGE
*/
@Injectable({
  providedIn: 'root'
})
export class MyLocalDB extends Dexie {
  travel_destination: Dexie.Table<TravelDestination, number>;

  constructor() {
    super('MyDB');

    this.version(1).stores({
      travel_destination: '++id, name'
    });
  }

}


/*
INIT
*/
export function appInit(apiClient: ApiClient): () => Promise<any> {
  return () => apiClient.init();
}

/*
API CONFIG
*/
export interface AppConfig {
  apiEndpoint: string;
}

const APP_CONFIG_VALUE = {
  apiEndpoint: 'http://localhost:3003'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('TOKEN_API_CONFIG');



/*
STATE
*/
export interface AppState {
  travelDestination: TravelDestinationsState;
  tracking: TrackingState;
}

const initAppState = {
  travelDestination: initTravelDestination(),
  tracking: initTrackingState()
}

const appReducers: ActionReducerMap<AppState> = {
  travelDestination: travelDestinationReducer,
  tracking: trackingReducer
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MapComponent,
    TrackingDisplayComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    TravelDestinationModule,
    NGRXModule.forRoot(
      appReducers,
      {
        initialState: initAppState, runtimeChecks: {
          strictStateImmutability: false,
          strictActionImmutability: false
        }
      }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    MyLocalDB,
    AuthService,
    ApiClientLog,
    { provide: ApiClientImpl, useClass: ApiClientLog },
    { provide: ApiClient, useExisting: ApiClientImpl },
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: APP_INITIALIZER, useFactory: appInit, deps: [ApiClient], multi: true },
    TrackingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
