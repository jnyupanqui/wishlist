import { Injectable, Inject, forwardRef } from '@angular/core';
import { TravelDestination } from '../models/travel-destination.model';
import { Store } from '@ngrx/store';
import { AppState, AppConfig, APP_CONFIG } from '../app.module';
import { NewTavelDestination, InitTravelDestination, TravelDestinationsState, TravelDestinationActionTypes, travelDestinationReducer, SelectFavoriteDestination } from '../models/travel-destination-state';

import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';

import { MyLocalDB } from '../app.module';
import { ApiClient } from './api.client';

@Injectable()
export class ApiClientImpl extends ApiClient {

    private travelDestination: TravelDestination[];

    constructor(private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private appConfig: AppConfig,
        private http: HttpClient,
        private myDB: MyLocalDB) {
        super();
        this.store.select(state => state.travelDestination.travelDestinations)
            .subscribe((travelDestination: TravelDestination[]) => {
                if (travelDestination) {
                    this.travelDestination = travelDestination;
                }
            });
    }

    async init(): Promise<any> {
        const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
        const req = new HttpRequest('GET', this.appConfig.apiEndpoint + '/destinos', { headers: headers });
        const response: any = await this.http.request(req).toPromise();

        this.store.dispatch(new InitTravelDestination(response.body));
    }

    getAllTravelDestination(): TravelDestination[] {
        return this.travelDestination;
    }

    addTravelDestination(travelDestination: TravelDestination) {
        const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
        const req = new HttpRequest('POST', this.appConfig.apiEndpoint + '/destinos', travelDestination, { headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status == 200) {
                this.store.dispatch(new NewTavelDestination(travelDestination));

                this.myDB.travel_destination.add(travelDestination);
            }
        });

    }

    setFavorite(travelDestination: TravelDestination) {
        this.store.dispatch(new SelectFavoriteDestination(travelDestination));
    }

    removeTravelDestination(travelDestination: TravelDestination) {

    }

}