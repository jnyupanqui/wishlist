import { ApiClientImpl } from './api-impl.client';
import { Injectable } from '@angular/core';
import { TravelDestination } from '../models/travel-destination.model';

@Injectable()
export class ApiClientLog extends ApiClientImpl {

    addTravelDestination(travelDestination: TravelDestination) {
        console.log('Agregando nuevo destino de viaje...');
        super.addTravelDestination(travelDestination);
        console.log(JSON.stringify(travelDestination) + ' fue agregado.');
    }
    
}