import { TravelDestination } from '../models/travel-destination.model';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class ApiClient {

    abstract init(): Promise<any>;

    abstract getAllTravelDestination(): TravelDestination[];

    abstract addTravelDestination(travelDestination: TravelDestination): void;

    abstract removeTravelDestination(travelDestination: TravelDestination): void;

    abstract setFavorite(travelDestination: TravelDestination): void;

}