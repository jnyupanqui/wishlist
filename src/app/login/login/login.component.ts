import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  badCredentials = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  login(username: string, password: string) {
    this.authService.authenticate(username, password);
    if (!this.authService.isAuthenticated()) {
      this.badCredentials = true;
      setInterval(() => this.badCredentials = false, 3000);
    }
  }

  logout() {
    this.authService.deAuthenticate();
  }

  isLogged(): boolean {
    return this.authService.isAuthenticated();
  }

}
