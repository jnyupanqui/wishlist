import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapa',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  styles: [`
    mgl-map {
      height: 100vh;
      width: 100vw;
    }
  `]
})
export class MapComponent implements OnInit {

  style = {
    sources:{
      world:{
        type:'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version:8,
    layers:[{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor() { }

  ngOnInit(): void {
  }

}
