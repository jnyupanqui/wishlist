import { TrackingState, initTrackingState, AddTrackClick, TrackingClick, trackingReducer } from './tracking-state';

describe('Reducers Tracking', () => {
    
    it('Should reduce add new track', () => {
        const prevState: TrackingState = initTrackingState();
        const tag1 = new TrackingClick('#tag1');
        const action: AddTrackClick = new AddTrackClick(tag1);
        const newState = trackingReducer(prevState, action);

        expect(newState.trackingClicks[0].tag).toEqual('#tag1');
        expect(newState.trackingClicks.length).toEqual(1);
        expect(newState.trackingClicks[0].counter).toEqual(1);
    })
    it('Should reduce counter track', () => {
        const prevState: TrackingState = initTrackingState();
        const tag1 = new TrackingClick('#tag1');
        const pressElementTracked: AddTrackClick = new AddTrackClick(tag1);
        const changeState = trackingReducer(prevState, pressElementTracked);
        const newState = trackingReducer(changeState, pressElementTracked);

        expect(newState.trackingClicks[0].tag).toEqual('#tag1');
        expect(newState.trackingClicks.length).toEqual(1);
        expect(newState.trackingClicks[0].counter).toEqual(2);
    })
})