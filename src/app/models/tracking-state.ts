import { Action } from '@ngrx/store';

export class TrackingClick {

    counter: number;

    constructor(public tag: string) {
        this.counter = 0;
    }

    increase() {
        this.counter++;
    }
}

export interface TrackingState {
    trackingClicks: TrackingClick[];
}

export const initTrackingState = function () {
    return {
        trackingClicks: []
    }
}

export enum TrackingActionTypes {
    ADD_TRACK_CLICK = '[Tracking] Add Track'
}

export class AddTrackClick implements Action {
    type = TrackingActionTypes.ADD_TRACK_CLICK;
    constructor(public trackingClick: TrackingClick) { }
}

export type TrackingAction = AddTrackClick;

export function trackingReducer(
    state: TrackingState,
    action: TrackingAction): TrackingState {

    switch (action.type) {
        case TrackingActionTypes.ADD_TRACK_CLICK: {
            const flat = (action as AddTrackClick).trackingClick;
            let found = state.trackingClicks.find(tracking => tracking.tag == flat.tag);
            if (!found) {
                found = flat;
                state.trackingClicks = [...state.trackingClicks, found];
            }
            found.increase();
            return {
                ...state
            }
        }
        default:
            return state;
    }
}