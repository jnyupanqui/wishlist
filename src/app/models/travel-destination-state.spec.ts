import { TravelDestination } from "./travel-destination.model"
import { initTravelDestination, NewTavelDestination, TravelDestinationsState, travelDestinationReducer, SelectFavoriteDestination, InitTravelDestination } from './travel-destination-state'

describe('Reducers Travel Destination', () => {
    const barcelona = new TravelDestination('Barcelona');
    const travelDestinations = [
                new TravelDestination('Bogota'),
                new TravelDestination('Lima'),
                new TravelDestination('Quito')
            ];
    const prevState: TravelDestinationsState = initTravelDestination();
    it('Should reduce new travel destination', () => {
        const action: NewTavelDestination = new NewTavelDestination(barcelona);
        const newState = travelDestinationReducer(prevState, action);

        expect(newState.travelDestinations[0].name).toEqual('Barcelona');
        expect(newState.travelDestinations.length).toEqual(1);
    })
    it('Should reduce select favorite travel destination', () => {
        const action: NewTavelDestination = new NewTavelDestination(barcelona);
        const changedState = travelDestinationReducer(prevState, action);
        const newState = travelDestinationReducer(changedState, new SelectFavoriteDestination(barcelona));

        expect(newState.travelDestinations.pop().getIsFavorite()).toBeTrue();
    })
    it('Should reduce initialize travel destination', () => {
        const action: InitTravelDestination = new InitTravelDestination(travelDestinations);
        const newState = travelDestinationReducer(prevState, action);

        expect(newState.travelDestinations[0].name).toEqual('Bogota');
        expect(newState.travelDestinations.length).toEqual(3);
    })
})