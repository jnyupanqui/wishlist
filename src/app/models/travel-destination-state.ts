import { TravelDestination } from "./travel-destination.model";
import { Action } from '@ngrx/store';

export interface TravelDestinationsState {
    travelDestinations: TravelDestination[]
}

export const initTravelDestination = function() {
    return {
        travelDestinations: []
    }
}

export enum TravelDestinationActionTypes {
    NEW_TRAVEL_DESTINATION = '[Travel Destination] New',
    INIT_TRAVEL_DESTINATION = '[Travel Destination] Init',
    SELECT_FAVORITE_DESTINATION = '[Travel Destination] FAVORITE'
}

export class NewTavelDestination implements Action {
    type = TravelDestinationActionTypes.NEW_TRAVEL_DESTINATION;
    constructor(public travelDestination: TravelDestination) { }
}

export class InitTravelDestination implements Action {
    type = TravelDestinationActionTypes.INIT_TRAVEL_DESTINATION;
    constructor(public travelDestinations: TravelDestination[]) { }
}

export class SelectFavoriteDestination implements Action {
    type = TravelDestinationActionTypes.SELECT_FAVORITE_DESTINATION;
    constructor(public travelDestination: TravelDestination) { }
}

export type TravelDestinationAction = NewTavelDestination | InitTravelDestination | SelectFavoriteDestination;

export function travelDestinationReducer(
    state: TravelDestinationsState,
    action: TravelDestinationAction
): TravelDestinationsState {
    switch (action.type) {
        case TravelDestinationActionTypes.NEW_TRAVEL_DESTINATION: {
            return {
                ...state,
                travelDestinations: [...state.travelDestinations, (action as NewTavelDestination).travelDestination]
            }
        }
        case TravelDestinationActionTypes.INIT_TRAVEL_DESTINATION: {
            return {
                ...state,
                travelDestinations: (action as InitTravelDestination).travelDestinations.map(item => new TravelDestination(item.name))
            }
        }
        case TravelDestinationActionTypes.SELECT_FAVORITE_DESTINATION: {
            state.travelDestinations.forEach((item: TravelDestination) => item.setIsFavorite(false));
            (action as SelectFavoriteDestination).travelDestination.setIsFavorite(true);
            return {
                ...state
            }
        }
        default:
            return state;
    }
}
