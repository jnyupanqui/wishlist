export class TravelDestination {
    id: number;
    private isFavorite: boolean;
    constructor(public name: string) {
        this.isFavorite = false;
    }

    getIsFavorite(): boolean {
        return this.isFavorite;
    }

    setIsFavorite(isFavorite: boolean) {
        this.isFavorite = isFavorite;
    }
}