import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  authenticate(username: string, password: string) {
    if(username == 'user' && password == 'pass') {
      localStorage.setItem('user', username);
    }
  }

  private getUser(): string {
    return localStorage.getItem('user');
  }

  isAuthenticated(): boolean {
    if(this.getUser()) {
      return true;
    }
    return false;
  }

  deAuthenticate() {
    localStorage.removeItem('user');
  }
}
