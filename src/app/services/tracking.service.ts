import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { TrackingClick, AddTrackClick } from '../models/tracking-state';

@Injectable()
export class TrackingService {

    private trackingClicks = [];

    constructor(private store: Store<AppState>) {
        store.select(state => state.tracking.trackingClicks)
        .subscribe((trackingClicks: TrackingClick[]) => {
            if(trackingClicks != null) {
                this.trackingClicks = trackingClicks;
            }
        });
    }

    getTrackingClick(): TrackingClick[] {
        return this.trackingClicks;
    }

    addTrackingClick(tags: string[]) {
        tags.map(tag => new TrackingClick(tag))
        .forEach(track => {
            this.store.dispatch(new AddTrackClick(track))
        });
        
    }
}