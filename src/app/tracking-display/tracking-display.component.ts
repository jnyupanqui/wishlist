import { Component, OnInit } from '@angular/core';
import { TrackingService } from '../services/tracking.service';
import { TrackingClick } from '../models/tracking-state';

@Component({
  selector: 'app-tracking-display',
  templateUrl: './tracking-display.component.html',
  styleUrls: ['./tracking-display.component.css']
})
export class TrackingDisplayComponent implements OnInit {

  constructor(private trackingService: TrackingService) { }

  ngOnInit(): void {
  }

  getAllTracking(): TrackingClick[] {
    return this.trackingService.getTrackingClick();
  }

}
