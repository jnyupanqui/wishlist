import { Component, OnInit, Input } from '@angular/core';
import { TravelDestination } from 'src/app/models/travel-destination.model';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ApiClient } from 'src/app/client/api.client';

@Component({
  selector: 'app-travel-destination-details',
  templateUrl: './travel-destination-details.component.html',
  styleUrls: ['./travel-destination-details.component.css'],
  animations: [
    trigger('isFavorite', [
      state('setFavoriteTrue', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('setFavoriteFalse', style({
        backgroundColor: 'White'
      })),
      transition('setFavoriteTrue => setFavoriteFalse', [
        animate('3s')
      ]),
      transition('setFavoriteFalse => setFavoriteTrue', [
        animate('1s')
      ])
    ])
  ]
})
export class TravelDestinationDetailsComponent implements OnInit {

  @Input()
  travelDestination: TravelDestination;

  constructor(private apiClient: ApiClient) { }

  ngOnInit(): void {
  }

  select(): void {
    this.apiClient.setFavorite(this.travelDestination);
  }

  isFavorite(): boolean {
    return this.travelDestination.getIsFavorite();
  }

}
