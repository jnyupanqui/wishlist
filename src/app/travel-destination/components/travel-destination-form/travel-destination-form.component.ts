import { Component, OnInit } from '@angular/core';

import { ApiClient } from '../../../client/api.client';
import { TravelDestination } from 'src/app/models/travel-destination.model';

@Component({
  selector: 'app-travel-destination-form',
  templateUrl: './travel-destination-form.component.html',
  styleUrls: ['./travel-destination-form.component.css']
})
export class TravelDestinationFormComponent implements OnInit {

  validRequired = true;

  constructor(private apiClient: ApiClient) { }

  ngOnInit(): void {
  }

  add(name: string) {
    this.validRequired = false;
    if(name.trim().length) {
      this.apiClient.addTravelDestination(new TravelDestination(name));
      this.validRequired = true;
    }
    (<HTMLInputElement>document.getElementById('inputName')).value = '';
  }

}
