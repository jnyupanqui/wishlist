import { Component, OnInit } from '@angular/core';

import { ApiClient } from '../../../client/api.client';
import { TravelDestination } from 'src/app/models/travel-destination.model';

@Component({
  selector: 'app-travel-destination-list',
  templateUrl: './travel-destination-list.component.html',
  styleUrls: ['./travel-destination-list.component.css']
})
export class TravelDestinationListComponent implements OnInit {

  constructor(private apiCLient: ApiClient) { }

  ngOnInit(): void {
  }

  getAll(): TravelDestination[] {
    return this.apiCLient.getAllTravelDestination();
  }

}
