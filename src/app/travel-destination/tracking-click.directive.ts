import { Directive, ElementRef, Input } from '@angular/core';
import { TrackingService } from '../services/tracking.service';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appOtradirectiva]'
})
export class TrackingClickDirective {

  @Input()
  trackingTag: string;

  private element: HTMLHtmlElement;

  constructor(private elementRef: ElementRef, private trackingService: TrackingService) {
    this.element = elementRef.nativeElement;
    fromEvent(this.element, 'click')
      .subscribe(event => this.track(event));
  }

  private track(event: Event): void {
    const trackingTags = this.trackingTag || this.element.attributes.getNamedItem('trackingTag').value;
    this.trackingService.addTrackingClick(trackingTags.split(' '))
  }

}
