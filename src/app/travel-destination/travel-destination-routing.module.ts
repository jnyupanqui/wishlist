import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ManageComponent } from './components/manage/manage.component';

import { LoginGuard } from '../guards/login/login.guard';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'manage', component: ManageComponent, canActivate: [LoginGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelDestinationRoutingModule { }
