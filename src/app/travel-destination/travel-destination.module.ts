import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TravelDestinationRoutingModule } from './travel-destination-routing.module';
import { TravelDestinationFormComponent } from './components/travel-destination-form/travel-destination-form.component';
import { TravelDestinationListComponent} from './components/travel-destination-list/travel-destination-list.component';
import { TravelDestinationDetailsComponent} from './components/travel-destination-details/travel-destination-details.component';
import { HomeComponent } from './components/home/home.component';
import { ManageComponent } from './components/manage/manage.component';
import { TrackingClickDirective } from './tracking-click.directive';

@NgModule({
  declarations: [
    TravelDestinationFormComponent, 
    TravelDestinationListComponent,
    TravelDestinationDetailsComponent,
    HomeComponent, 
    ManageComponent, TrackingClickDirective
  ],
  imports: [
    CommonModule,
    TravelDestinationRoutingModule
  ]
})
export class TravelDestinationModule { }
